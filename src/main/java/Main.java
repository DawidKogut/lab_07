import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        User user = new User("Dawid", "Kogut", "Student", Arrays.asList("2015 - 2018 I Liceum Ogolnoksztalcace Dabrowa Tarnowska", "2018 - 2022 PWSZ Tarnow"), "I am a third-year student of engineering studies at Panstwowa Wyzsza Szkola Zawodowa in Tarnow. Im trying my best to become a developer. ");
        String filename = "resume.pdf";
        PdfPrinter pdfPrinter = new PdfPrinter(filename, user);
        pdfPrinter.print();
    }
}